from django.conf.urls import url, include

from .admin import photo_album_admin
from .views import APIHome

urlpatterns = [
    url(r'^admin/', photo_album_admin.urls),
    url(r'^api/?$', APIHome.as_view(), name='api-home'),
    url(r'^api/auth/', include('users.urls')),
    url(r'^api/', include('images.urls')),
]
