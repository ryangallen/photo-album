from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView


class APIHome(APIView):
    '''
    ## Endpoints

    <table>
      <tr><th>Name</th><th>URL</th></tr>
      <tr>
        <td>albums-list</td>
        <td><a href='/api/albums'>/api/albums</a></td>
      </tr>
      <tr>
        <td>albums-detail</td>
        <td>/api/albums/&lt;album_pk&gt;</td>
      </tr>
      <tr>
        <td>albums-order</td>
        <td>/api/albums/&lt;album_pk&gt;/order</td>
      </tr>
      <tr>
        <td>album-images-list</td>
        <td>/api/albums/&lt;album_pk&gt;/images</td>
      </tr>
      <tr>
        <td>album-images-detail</td>
        <td>/api/albums/&lt;album_pk&gt;/images/&lt;album_image_pk&gt;</td>
      </tr>
      <tr>
        <td>orders-list</td>
        <td><a href='/api/orders'>/api/orders</a></td>
      </tr>
      <tr>
        <td>orders-detail</td>
        <td>/api/orders/&lt;order_pk&gt;</td>
      </tr>
      <tr>
        <td>orders-download</td>
        <td>/api/orders/&lt;order_pk&gt;/download</td>
      </tr>
      <tr>
        <td>order-downloads-list</td>
        <td>/api/orders/&lt;order_pk&gt;/downloads</td>
      </tr>
      <tr>
        <td>order-downloads-detail &nbsp; &nbsp;</td>
        <td>/api/orders/&lt;order_pk&gt;/downloads/&lt;download_pk&gt;</td>
      </tr>
    </table>
    '''
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        return Response()
