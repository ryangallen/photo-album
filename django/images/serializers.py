from drf_extra_fields.fields import Base64ImageField
from rest_framework.exceptions import ValidationError
from rest_framework.serializers import (ModelSerializer, SerializerMethodField,
                                        ReadOnlyField, DateTimeField)
from rest_framework.validators import UniqueValidator

from .models import Album, AlbumImage, Download, Order, SourceImage


class AlbumSerializer(ModelSerializer):
    owner = ReadOnlyField(source='owner.pk')

    class Meta:
        model = Album
        fields = '__all__'


class SourceImageSerializer(ModelSerializer):
    image = Base64ImageField(max_length=70)
    timestamp = DateTimeField(write_only=True)

    class Meta:
        model = SourceImage
        fields = ('image', 'origin', 'updated_at', 'timestamp')
        extra_kwargs = {'album_id': {'write_only': True}}

    def ignore_uniqueness_validation(self):
        for field in self.fields.values():
            for i, validator in enumerate(field.validators):
                if type(validator) is UniqueValidator:
                    del field.validators[i]
                    break

    def to_internal_value(self, data):
        try:
            source = SourceImage.objects.get(origin=data['origin'])
            self.ignore_uniqueness_validation()
            internal_value = super().to_internal_value(data)

            timestamp = internal_value.pop('timestamp')
            if timestamp and timestamp > source.updated_at:
                for key, value in internal_value.items():
                    setattr(source, key, value)
                source.save()
            else:
                if SourceImage.objects.filter(albumimage__album__id=data['album_id']).exists():
                    raise ValidationError(['image is already added to the album.'])

        except SourceImage.DoesNotExist:
            internal_value = super().to_internal_value(data)
            internal_value.pop('timestamp')
            source = SourceImage.objects.create(**internal_value)
        return source


class ImageSerializer(ModelSerializer):
    '''AlbumImage and SourceImage details handled as a single object'''
    source = SourceImageSerializer()

    class Meta:
        model = AlbumImage
        fields = ('id', 'album', 'source')

    def flatten_source(self, data):
        source_data = data.pop('source')
        for key in source_data:
            data[key] = source_data[key]
        return data

    def nest_source(self, data):
       source_keys = list(SourceImageSerializer.Meta.fields) +\
                     list(SourceImageSerializer.Meta.extra_kwargs.keys())
       data['source'] = {}
       for key in source_keys:
           if key in data:
               data['source'][key] = data.pop(key)
       return data

    def to_internal_value(self, data):
        data = self.nest_source(data)
        return super().to_internal_value(data)

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        return self.flatten_source(representation)

    def update(self, instance, validated_data):
        for attr, value in validated_data.pop('source').items():
            setattr(instance.source, attr, value)
            instance.source.save()

        return super().update(instance, validated_data)


class OrderSerializer(ModelSerializer):
    download_count = SerializerMethodField()

    class Meta:
        model = Order
        fields = '__all__'

    def get_download_count(self, obj):
        return obj.downloads.count()


class DownloadSerializer(ModelSerializer):

    class Meta:
        model = Download
        fields = '__all__'
