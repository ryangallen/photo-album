from django.contrib import admin

from .models import Album, AlbumImage, Download, Order, SourceImage
from main.admin import photo_album_admin


@admin.register(SourceImage, site=photo_album_admin)
class SourceImageAdmin(admin.ModelAdmin):
    pass


@admin.register(AlbumImage, site=photo_album_admin)
class AlbumImageAdmin(admin.ModelAdmin):
    pass


@admin.register(Album, site=photo_album_admin)
class AlbumAdmin(admin.ModelAdmin):
    pass


@admin.register(Order, site=photo_album_admin)
class OrderAdmin(admin.ModelAdmin):
    pass


@admin.register(Download, site=photo_album_admin)
class DownloadAdmin(admin.ModelAdmin):
    pass
