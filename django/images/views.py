from django.core.files import File
from django.http import HttpResponse
from django.utils.text import slugify
from rest_framework.decorators import detail_route
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet, ReadOnlyModelViewSet

from .models import Album, SourceImage, AlbumImage, Download, Order
from .serializers import (AlbumSerializer, ImageSerializer,
                          OrderSerializer, DownloadSerializer)
from utils.views import RandomIdModelViewsetMixin


class AlbumViewSet(RandomIdModelViewsetMixin, ModelViewSet):
    serializer_class = AlbumSerializer
    permission_classes = [IsAuthenticated,]

    def get_queryset(self):
        return Album.objects.filter(owner=self.request.user)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    @detail_route()
    def order(self, request, *args, **kwargs):
        order = self.get_object().create_order(request.user)
        return Response(OrderSerializer(order).data)


class ImageViewSet(RandomIdModelViewsetMixin, ModelViewSet):
    serializer_class = ImageSerializer
    permission_classes = [IsAuthenticated,]

    def get_queryset(self):
        return AlbumImage.objects\
            .filter(album=self.kwargs['album_pk'], album__owner=self.request.user).order_by('source__id')

    def create(self, request, *args, **kwargs):
        request.data['album'] = request.data['album_id'] = kwargs['album_pk']
        return super().create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        self.http_method_not_allowed(request, *args, **kwargs)


class OrderViewSet(RandomIdModelViewsetMixin, ReadOnlyModelViewSet):
    serializer_class = OrderSerializer
    permission_classes = [IsAuthenticated,]

    def get_queryset(self):
        return Order.objects.filter(ordered_by=self.request.user)

    @detail_route(methods=['get'])
    def download(self, request, *args, **kwargs):
        download = self.get_object().create_download(request.user)
        response = HttpResponse(File(download.order.files),
                                content_type='application/zip')
        response['Content-Disposition']  = \
            'attachment; filename={album_name}-{album_id}-{ordered_at}.zip'.format(
                album_name=slugify(download.order.album.name),
                album_id=download.order.album.id,
                ordered_at=slugify(download.order.ordered_at))
        return response


class DownloadViewSet(RandomIdModelViewsetMixin, ReadOnlyModelViewSet):
    serializer_class = DownloadSerializer
    permission_classes = [IsAuthenticated,]

    def get_queryset(self):
        return Download.objects.filter(order=self.kwargs['order_pk'],
                                       downloaded_by=self.request.user)
