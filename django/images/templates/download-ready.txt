Your album '{{ order.album }}' is available for download:

{{ host }}{{ order.files.url }}
