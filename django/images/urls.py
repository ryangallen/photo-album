from django.conf.urls import url, include

from .views import AlbumViewSet, ImageViewSet, OrderViewSet, DownloadViewSet
from utils import routers

'''
Image API URLs

albums:
    list:            settings.API_URL + '/albums',
    detail:          settings.API_URL + '/albums/{album_id}',
    detail_order:    settings.API_URL + '/albums/{album_id}/order',
    image_list:      settings.API_URL + '/albums/{album_id}/images',
    image_detail:    settings.API_URL + '/albums/{album_id}/images/{image_id}',

orders:
    list:            settings.API_URL + '/orders',
    detail:          settings.API_URL + '/orders/{order_id}',
    detail_download: settings.API_URL + '/orders/{order_id}/download',
    download_list:   settings.API_URL + '/orders/{order_id}/downloads',
    download_detail: settings.API_URL + '/orders/{order_id}/downloads/{download_id}',
'''

router = routers.OptionalSlashRouter()
router.register(r'albums', AlbumViewSet, base_name='albums')
images_router = routers.NestedOptionalSlashRouter(router, r'albums', lookup='album')
images_router.register(r'images', ImageViewSet, base_name='album-images')
router.register(r'orders', OrderViewSet, base_name='orders')
downloads_router = routers.NestedOptionalSlashRouter(router, r'orders', lookup='order')
downloads_router.register(r'downloads', DownloadViewSet, base_name='order-downloads')

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^', include(images_router.urls)),
    url(r'^', include(downloads_router.urls)),
]
