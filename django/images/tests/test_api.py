from base64 import b64encode
from datetime import datetime, timedelta
from json import loads
from os import path
from unittest import expectedFailure

from django.conf import settings
from django.contrib.auth import get_user
from django.core.management import call_command
from django.template.defaultfilters import date
from django.utils.six import StringIO
from django.utils.timezone import utc
from freezegun import freeze_time
from rest_framework.test import APITestCase

from images.models import Album, AlbumImage, Download, Order, SourceImage
from utils.utils import add_watermark


def iso8601(datetime_obj):
    '''http://www.django-rest-framework.org/api-guide/fields/#datetimefield'''
    return date(datetime_obj, 'c').split('+')[0] + 'Z'


test_timestamp = datetime(year=2017, month=9, day=1, hour=12, tzinfo=utc)
formatted_test_timestamp = iso8601(test_timestamp)
@freeze_time(test_timestamp)
class ImagesAPITest(APITestCase):

    def setUp(self):
        call_command('create_test_album', stdin=None, stdout=StringIO())
        self.client.login(email='test@' + settings.AUTH_EMAIL_DOMAINS[0],
                          password='testpassword1')
        self.user = get_user(self.client)
        self.album = Album.objects.first()
        self.album_images = self.album.images.all().order_by('source__id')
        self.order = Order.objects.first()

        self.urls = {
            'albums': {
                'list':            settings.API_URL + '/albums',
                'detail':          settings.API_URL + '/albums/{album_id}',
                'detail_order':    settings.API_URL + '/albums/{album_id}/order',
                'image_list':      settings.API_URL + '/albums/{album_id}/images',
                'image_detail':    settings.API_URL + '/albums/{album_id}/images/{image_id}',
            },
            'orders': {
                'list':            settings.API_URL + '/orders',
                'detail':          settings.API_URL + '/orders/{order_id}',
                'detail_download': settings.API_URL + '/orders/{order_id}/download',
                'download_list':   settings.API_URL + '/orders/{order_id}/downloads',
                'download_detail': settings.API_URL + '/orders/{order_id}/downloads/{download_id}',
            },
        }

        self.expected_album_json = {
            'created_at': formatted_test_timestamp,
            'id': self.album.id,
            'name': 'My Album',
            'owner': self.album.owner.pk,
            'updated_at': iso8601(test_timestamp)
        }
        self.expected_order_json = {
            'album': self.album.id,
            'files': 'http://testserver/media/orders/{order_id}.zip'.format(order_id=self.order.id),
            'id': self.order.id,
            'ordered_at': formatted_test_timestamp,
            'ordered_by': self.user.id,
            'download_count': 0
        }
        self.expected_images_json = [{
            'id': image.id,
            'album': self.album.id,
            'image': 'http://testserver/media/source-images/{source_image_id}.jpg'.format(
                source_image_id=image.source.id),
            'origin': 'http://www.example.com/test2.jpg',
            'updated_at': formatted_test_timestamp,
        } for i, image in enumerate(self.album_images)]

    def assertCount(self, model, expected_count, message=None, filter={}):
        return self.assertEqual(model.objects.filter(**filter).count(), expected_count, message)

    def test_album_list_get(self):
        response = self.client.get(self.urls['albums']['list'])

        response_content = loads(response.content)['results']
        self.assertEqual(len(response_content), 1)
        self.assertDictEqual(response_content[0], self.expected_album_json)
        self.assertEqual(response.status_code, 200)

    def test_album_list_post(self):
        response = self.client.post(self.urls['albums']['list'], {
            'name': 'test album',
        })

        response_content = loads(response.content)
        new_album = Album.objects.get(id=response_content.get('id'))
        self.assertDictEqual(response_content, {
            'id': new_album.id,
            'name': 'test album',
            'owner': self.user.pk,
            'created_at': formatted_test_timestamp,
            'updated_at': formatted_test_timestamp
        })
        self.assertEqual(response.status_code, 201)

    def test_album_detail_get(self):
        response = self.client.get(
            self.urls['albums']['detail'].format(album_id=self.album.id))

        response_content = loads(response.content)
        self.assertDictEqual(response_content, self.expected_album_json)
        self.assertEqual(response.status_code, 200)

    def test_album_detail_patch(self):
        updated_timestamp = test_timestamp + timedelta(hours=5)
        with freeze_time(updated_timestamp):
            response = self.client.patch(
                self.urls['albums']['detail'].format(album_id=self.album.id),
                {'name': 'New Album Name'})

            response_content = loads(response.content)
            self.expected_album_json['name'] = 'New Album Name'
            self.expected_album_json['updated_at'] = iso8601(updated_timestamp)
            self.assertDictEqual(response_content, self.expected_album_json)
            self.assertEqual(response.status_code, 200)

    def test_album_detail_delete(self):
        self.assertCount(Album, 1, filter={'id': self.album.id})
        response = self.client.delete(
            self.urls['albums']['detail'].format(album_id=self.album.id))

        self.assertFalse(response.content)

        self.assertEqual(response.status_code, 204)
        self.assertCount(Album, 0, 'the album should be deleted',
                         filter={'id': self.album.id})

    def test_album_detail_order_get(self):
        self.assertCount(Order, 1)
        response = self.client.get(self.urls['albums']['detail_order'].format(
            album_id=self.album.id))

        response_content = loads(response.content)
        new_order = Order.objects.get(id=response_content.get('id'))
        self.expected_order_json['id'] = new_order.id
        self.expected_order_json['files'] = '/media/orders/{order_id}.zip'.format(
            order_id=new_order.id)
        self.assertDictEqual(response_content, self.expected_order_json)

        self.assertEqual(response.status_code, 200)
        self.assertCount(Order, 2, 'a new order should be created')

    def test_album_image_list_get(self):
        response = self.client.get(self.urls['albums']['image_list'].format(
            album_id=self.album.id))

        results = loads(response.content)['results']
        for i, result in enumerate(results):
            self.assertDictEqual(result, self.expected_images_json[i])
        self.assertEqual(response.status_code, 200)

    def test_album_image_list_post_new_image(self):
        self.assertCount(AlbumImage, 4)
        self.assertCount(SourceImage, 4)
        img_file = open(path.join(settings.BASE_DIR, 'images/tests/data/img/test-watermark.gif'), 'rb')
        response = self.client.post(self.urls['albums']['image_list'].format(album_id=self.album.id), {
            'image': b64encode(img_file.read()).decode('utf-8'),
            'origin': 'http://i.imgur.com/test-watermark.gif',
            'timestamp': formatted_test_timestamp
        }, format='json')

        response_content = loads(response.content)
        new_image = AlbumImage.objects.get(id=response_content.get('id'))
        self.assertDictEqual(response_content, {
            'album': self.album.id,
            'id': new_image.id,
            'image': 'http://testserver/media/source-images/{source_id}.gif'.format(
                source_id=new_image.source.id),
            'origin': 'http://i.imgur.com/test-watermark.gif',
            'updated_at': formatted_test_timestamp,
        })

        self.assertEqual(response.status_code, 201)
        self.assertCount(AlbumImage, 5, 'a new album image should be created')
        self.assertCount(SourceImage, 5, 'a new source image should be created')

    def test_album_image_list_post_existing_image_in_same_album(self):
        self.assertCount(AlbumImage, 4)
        self.assertCount(SourceImage, 4)
        img_file = open(path.join(settings.BASE_DIR, 'images/tests/data/img/test-img.jpg'), 'rb')

        response = self.client.post(self.urls['albums']['image_list'].format(album_id=self.album.id), {
            'image': b64encode(img_file.read()).decode('utf-8'),
            'origin': 'http://www.example.com/test3.jpg',
            'timestamp': formatted_test_timestamp
        }, format='json')
        self.assertDictEqual(loads(response.content), {
            'source': ['image is already added to the album.']})

        self.assertEqual(response.status_code, 400)
        self.assertCount(AlbumImage, 4, 'a new album image should not be created')
        self.assertCount(SourceImage, 4, 'a new source image should not be created')

    def test_album_image_list_post_existing_image_in_different_album(self):
        new_album = Album.objects.create(name='My Other Album', owner=self.user)
        self.assertCount(AlbumImage, 4)
        self.assertCount(SourceImage, 4)
        img_file = open(path.join(settings.BASE_DIR, 'images/tests/data/img/test-img.jpg'), 'rb')
        response = self.client.post(self.urls['albums']['image_list'].format(album_id=new_album.id), {
            'image': b64encode(img_file.read()).decode('utf-8'),
            'origin': 'http://www.example.com/test4.jpg',
            'timestamp': formatted_test_timestamp
        }, format='json')

        response_content = loads(response.content)
        new_image = AlbumImage.objects.get(id=response_content.get('id'))
        self.assertDictEqual(response_content, {
            'album': new_album.id,
            'id': new_image.id,
            'image': 'http://testserver/media/source-images/{source_id}.jpg'.format(
                source_id=new_image.source.id),
            'origin': 'http://www.example.com/test5.jpg',
            'updated_at': formatted_test_timestamp,
        })

        self.assertEqual(response.status_code, 201)
        self.assertCount(AlbumImage, 5, 'a new album image should be created')
        self.assertCount(SourceImage, 4, 'a new source image should not be created')

    def test_album_image_list_post_existing_but_updated_source_image(self):
        '''
        Currently the API does not actually compare the images for differences
        but only checks the timestamp sent with the image against the
        updated_at field of any existing source image. This functionality may
        be sufficient for this project but could be expanded in the future to
        compare the image base64 encodings as well or instead.
        '''
        self.assertCount(AlbumImage, 4)
        self.assertCount(SourceImage, 4)

        # get stored source image
        existing_img_file = self.album_images[0].source.image
        existing_img_b64 = b64encode(existing_img_file.read()).decode('utf-8')

        # ensure stored source image matches original file
        original_img_path = path.join(settings.BASE_DIR, 'images/tests/data/img/test-img.jpg')
        original_img_file = open(original_img_path, 'rb')
        original_img_b64 = b64encode(original_img_file.read()).decode('utf-8')
        self.assertEqual(existing_img_b64, original_img_b64)

        # simulate an updated source image
        watermark_path = path.join(settings.BASE_DIR, 'images/tests/data/img/test-watermark.gif')
        updated_img_bytesio = add_watermark(original_img_path, watermark_path)
        updated_img_b64 = b64encode(updated_img_bytesio.getvalue()).decode('utf-8')
        self.assertNotEqual(existing_img_b64, updated_img_b64)

        updated_timestamp = test_timestamp + timedelta(days=3)
        with freeze_time(updated_timestamp):
            response = self.client.post(
                self.urls['albums']['image_list'].format(album_id=self.album.id), {
                    'image': updated_img_b64,
                    'origin': 'http://www.example.com/test6.jpg',
                    'timestamp': iso8601(updated_timestamp)
                }, format='json')

        response_content = loads(response.content)
        new_image = AlbumImage.objects.get(id=response_content.get('id'))
        self.assertDictEqual(response_content, {
            'album': self.album.id,
            'id': new_image.id,
            'image': 'http://testserver/media/source-images/{source_id}.jpg'.format(
                source_id=new_image.source.id),
            'origin': 'http://www.example.com/test7.jpg',
            'updated_at': iso8601(updated_timestamp),
        })

        self.assertEqual(response.status_code, 201)
        self.assertCount(AlbumImage, 5, 'a new album image should be created')
        self.assertCount(SourceImage, 4, 'a new source image should not be created')

    def test_album_image_detail_get(self):
        response = self.client.get(self.urls['albums']['image_detail'].format(
            album_id=self.album.id, image_id=self.album_images[0].id))
        response_content = loads(response.content)
        self.assertDictEqual(response_content, {
            'album': self.album.id,
            'id': self.album_images[0].id,
            'image': 'http://testserver/media/source-images/{source_id}.jpg'.format(
                source_id=self.album_images[0].source.id),
            'origin': 'http://www.example.com/test1.jpg',
            'updated_at': formatted_test_timestamp,
        self.assertEqual(response.status_code, 200)

    def test_album_image_detail_patch(self):
        response = self.client.patch(
            self.urls['albums']['image_detail'].format(album_id=self.album.id)
        self.assertDictEqual(loads(response.content),
                             {'detail': 'Method "PATCH" not allowed.'})
        self.assertEqual(response.status_code, 405)

    @expectedFailure
    def test_album_image_detail_delete(self):
        self.fail('incomplete test: assert expected response content')
        response = self.client.delete(self.urls['albums']['image_detail'].format(
            album_id=self.album.id, image_id=self.album_images[0].id))
        self.assertEqual(response.status_code, 204)

    @expectedFailure
    def test_order_list_get(self):
        self.fail('incomplete test: assert expected response content')
        response = self.client.get(self.urls['orders']['list'])
        self.assertEqual(response.status_code, 200)

    @expectedFailure
    def test_order_detail_get(self):
        self.fail('incomplete test: assert expected response content')
        response = self.client.get(self.urls['orders']['detail'].format(
            order_id=self.order.id))
        self.assertEqual(response.status_code, 200)

    @expectedFailure
    def test_order_detail_download_get(self):
        self.fail('incomplete test: assert expected response content')
        response = self.client.get(self.urls['orders']['detail_download'].format(
            order_id=self.order.id))
        self.assertEqual(response.status_code, 200)

    @expectedFailure
    def test_order_download_list_get(self):
        self.fail('incomplete test: assert expected response content')
        response = self.client.get(self.urls['orders']['download_list'].format(
            order_id=self.order.id))
        self.assertEqual(response.status_code, 200)

    @expectedFailure
    def test_order_download_detail_get(self):
        self.fail('incomplete test: assert expected response content')
        response = self.client.get(self.urls['orders']['download_detail'].format(
            order_id=self.order.id, download_id=self.order.downloads.first().id))
        self.assertEqual(response.status_code, 200)

    @expectedFailure
    def test_endpoints_only_return_data_pertinent_to_request_user(self):
        self.fail('incomplete test')
