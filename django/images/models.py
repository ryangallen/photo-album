from os import path
from zipfile import ZipFile, ZIP_DEFLATED

from django.conf import settings
from django.core.files.uploadedfile import SimpleUploadedFile
from django.db import models
from django.template.defaultfilters import date
from django.utils.six import BytesIO
from django.utils.text import slugify

from utils.models import RandomIDModel
from utils.utils import send_email


def media_storage_path(instance, filename):
    '''
    Callable function which returns a path for media files to be uploaded to.
    Examples:
        source-images/qAv4Vmv.png
        downloads/w1i4dbQ.zip
    '''
    dir_name = slugify(str(instance._meta.verbose_name_plural))
    new_file_name = '{id}.{ext}'.format(id=instance.id, ext=filename.split('.')[-1])
    return path.join(dir_name, new_file_name)


class SourceImage(RandomIDModel):
    image = models.ImageField(upload_to=media_storage_path, max_length=70,
                              help_text="stored copy of the original image")
    origin = models.URLField(max_length=2048, unique=True,
                             help_text="original image URL")
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.id


class AlbumImage(RandomIDModel):
    album = models.ForeignKey('Album', related_name='images', on_delete=models.CASCADE)
    source = models.ForeignKey('SourceImage', on_delete=models.PROTECT)

    def __str__(self):
        return '{} - {}'.format(self.album, self.source)


class Album(RandomIDModel):
    name = models.CharField(max_length=160)
    owner = models.ForeignKey('users.User', on_delete=models.PROTECT)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{} ({})'.format(', '.join([self.name, str(self.owner)]), self.id)

    def create_order(self, user):
        return Order.objects.create(album=self, ordered_by=user)


class OrderManager(models.Manager):

    def create(self, **kwargs):
        byte_stream = BytesIO()
        album_zip = ZipFile(byte_stream, mode='w', compression=ZIP_DEFLATED)
        for image in  kwargs['album'].images.all():
            arcname = '{name}.{ext}'.format(
                name=slugify(str(image.source)),
                ext=image.source.image.name.split('.')[-1])
            album_zip.write(image.source.image.path, arcname=arcname)
        album_zip.close()

        album_zip_upload = SimpleUploadedFile("album.zip", byte_stream.getvalue())
        order = super().create(**dict(kwargs, files=album_zip_upload))

        if kwargs['ordered_by'].email:
            send_email(
                subject='Your album is ready for download',
                context={'host': settings.HOST, 'order': order},
                recipients=[kwargs['album'].owner.email],
                txt_template='download-ready.txt',
                html_template='download-ready.html')

        return order


class Order(RandomIDModel):
    album = models.ForeignKey('Album')
    ordered_at = models.DateTimeField(auto_now_add=True)
    ordered_by = models.ForeignKey('users.User', on_delete=models.PROTECT)
    files = models.FileField(upload_to=media_storage_path, max_length=70,
                             help_text="stored file bundle of all images in "
                                       "the album, available for download")

    objects = OrderManager()

    def __str__(self):
        return '{} ({} at {})'.format(
            self.album.id, self.ordered_by,
            date(self.ordered_at, settings.DATETIME_FORMAT))

    def create_download(self, user):
        return Download.objects.create(order=self, downloaded_by=user)


class Download(RandomIDModel):
    order = models.ForeignKey('Order', related_name='downloads', on_delete=models.CASCADE)
    downloaded_at = models.DateTimeField(auto_now_add=True)
    downloaded_by = models.ForeignKey('users.User', on_delete=models.PROTECT)

    def __str__(self):
        return '{} ({} at {})'.format(
            self.order.album.id, self.downloaded_by,
            date(self.downloaded_at, settings.DATETIME_FORMAT))
