from django.core.validators import RegexValidator
from django.db import IntegrityError, models
from django.utils.crypto import get_random_string

ID_LENGTH = 7
ID_REGEX = '[0-9a-zA-Z]{{{len}}}'.format(len=ID_LENGTH)


def random_alphanumeric_id(length=ID_LENGTH):
    try:
        return get_random_string(length)
    except IntegrityError:
        return random_alphanumeric_id(length)


alphanumeric_id_validator = RegexValidator(
    regex='^{regex}$'.format(regex=ID_REGEX),
    message='ID must be {len} alphanumeric characters'.format(len=ID_LENGTH)
)


class RandomIDModel(models.Model):

    id = models.CharField(primary_key=True, editable=False,
                          default=random_alphanumeric_id, max_length=ID_LENGTH,
                          validators=[alphanumeric_id_validator])

    class Meta:
        abstract = True
