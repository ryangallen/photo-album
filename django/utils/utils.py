from io import BytesIO

from django.conf import settings
from django.core.mail import send_mail
from django.template.loader import get_template
from PIL import Image, ImageEnhance
from premailer import transform as css_inline

def send_email(subject, context, recipients, txt_template, html_template=None):
    txt_message = get_template(txt_template).render(context)
    html_message = css_inline(
        get_template(html_template).render(context)) if html_template else None

    send_mail(
        subject=subject,
        message=txt_message,
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=[recipients],
        html_message=html_message)

def suffix_filepath(path, suffix):
    if not suffix:
        return path
    split_path = path.split('.')
    if len(split_path) is 1:
        split_path.append('')
    return '{path}{extension}'.format(
        path='-'.join(['.'.join(split_path[0:-1]), suffix]),
        extension='.' + split_path[-1] if split_path[-1] else '')

def add_watermark(image_path, watermark_path, opacity=.35, output_path=None, save=False):
    image = Image.open(image_path).convert('RGB')
    watermark = Image.open(watermark_path).convert('RGBA')

    overlay = Image.new('RGBA', image.size, (255, 255, 255, 1))
    overlay.paste(watermark, (10, 10), mask=watermark.split()[3])
    overlay.putalpha(ImageEnhance.Brightness(overlay.split()[3]).enhance(opacity))

    output = (output_path if output_path else suffix_filepath(image_path, 'watermarked')) if save else \
             BytesIO()
    Image.composite(overlay, image, overlay).save(output, 'JPEG')
    return output
