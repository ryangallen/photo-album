from datetime import datetime
from string import digits, ascii_lowercase, ascii_uppercase

from django.db import connection
from django.db.models.base import ModelBase
from django.test import TestCase

from utils.models import RandomIDModel

ALPHANUMERIC = digits + ascii_lowercase + ascii_uppercase


class ModelMixinTestCase(TestCase):
    """
    https://stackoverflow.com/a/42742738/1797103
    Base class for tests of model mixins. To use, subclass and specify
    the mixin class variable. A model using the mixin will be made
    available in self.model.
    """

    def setUp(self):
        # Create a dummy model which extends the mixin
        self.model = ModelBase(
            'Test{name}{timestamp}'.format(
                name=self.mixin.__name__,
                timestamp=str(datetime.now().timestamp()).replace('.', '')),
            (self.mixin,),
            {'__module__': self.mixin.__module__})
        # timestamp is a work-around for:
        #     django/db/models/base.py:325: RuntimeWarning: Model
        #     'utils.testrandomidmodel' was already registered

        # Create the schema for our test model
        with connection.schema_editor() as schema_editor:
            schema_editor.create_model(self.model)

    def tearDown(self):
        # Delete the schema for the test model
        with connection.schema_editor() as schema_editor:
            schema_editor.delete_model(self.model)


class RandomIDModelTest(ModelMixinTestCase):
    mixin = RandomIDModel

    def test_obj_has_random_7_char_alphanumeric_id(self):
        obj = self.model.objects.create()
        self.assertEqual(len(obj.id), 7)
        for char in obj.id:
            self.assertIn(char, ALPHANUMERIC)

    def test_obj_create_implicitly_assigns_an_id(self):
        obj = self.model.objects.create()
        self.assertTrue(obj.id)

    def test_obj_create_explicitly_assigns_an_id(self):
        obj = self.model.objects.create(id='tUv3xY2')
        self.assertEqual(obj.id, 'tUv3xY2')
