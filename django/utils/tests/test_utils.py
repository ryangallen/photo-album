from unittest import TestCase
from utils.utils import suffix_filepath


class UtilsTest(TestCase):

    def test_suffix_filepath(self):
        test_cases = [
            {'input': ('', ''), 'output': ''},
            {'input': ('dog', ''), 'output': 'dog'},
            {'input': ('dog', 'new'), 'output': 'dog-new'},
            {'input': ('dog.jpg', ''), 'output': 'dog.jpg'},
            {'input': ('dog.jpg', 'new'), 'output': 'dog-new.jpg'},
            {'input': ('dog.cat.jpg', 'new'), 'output': 'dog.cat-new.jpg'},
            {'input': ('dog.cat.bird.jpg', 'new'), 'output': 'dog.cat.bird-new.jpg'},
        ]
        for case in test_cases:
            self.assertEqual(suffix_filepath(*case['input']), case['output'])
