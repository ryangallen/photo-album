from rest_framework_nested.routers import NestedMixin, SimpleRouter


class OptionalSlashRouter(SimpleRouter):

    def __init__(self):
        self.trailing_slash = '/?'
        super(SimpleRouter, self).__init__()


class NestedOptionalSlashRouter(NestedMixin, OptionalSlashRouter):
    pass
