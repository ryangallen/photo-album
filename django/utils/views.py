from .models import ID_REGEX


class RandomIdModelViewsetMixin(object):
    lookup_value_regex = ID_REGEX
