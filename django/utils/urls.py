import re


def optional_trailing_slash(included_urls):
    for url in included_urls[0].urlpatterns:
        url.regex = re.compile(url.regex.pattern.replace('/$', '/?$'))
    return included_urls
