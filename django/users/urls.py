from django.conf.urls import url, include
from rest_framework_jwt import views

from utils.urls import optional_trailing_slash

urlpatterns = [
    url(r'^', optional_trailing_slash(include('rest_framework.urls', namespace='rest_framework'))),
    url(r'^', optional_trailing_slash(include('djoser.urls'))),
    url(r'^jwt-token/create/?', views.obtain_jwt_token, name='jwt-create'),
    url(r'^jwt-token/refresh/?', views.refresh_jwt_token, name='jwt-refresh'),
    url(r'^jwt-token/verify/?', views.verify_jwt_token, name='jwt-verify'),
    # TODO: use 'djoser.urls.jwt' once it is released instead of jwt-token urls above
    # url(r'^', include('djoser.urls.jwt')),
]
