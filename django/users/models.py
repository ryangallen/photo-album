from django.db import models
from django.conf import settings
from django.contrib.auth.models import (AbstractUser,
                                        Group as DefaultGroup,
                                        UserManager as DefaultUserManager)
from django.utils.translation import ugettext_lazy as _

from .fields import LimitedDomainsEmailField


class UserManager(DefaultUserManager):
    '''https://github.com/django/django/blob/master/django/contrib/auth/models.py#L131'''

    def _create_user(self, username, email, password, **extra_fields):
        if not email:
            raise ValueError('An email must be set')
        return super()._create_user(email, email, password, **extra_fields)

    def create_user(self, email, password=None, **extra_fields):
        return super().create_user(email, email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        return super().create_superuser(email, email, password, **extra_fields)


class User(AbstractUser):
    '''https://github.com/django/django/blob/master/django/contrib/auth/models.py#L288'''
    email = LimitedDomainsEmailField(_('email address'), unique=True,
                                     domains=getattr(settings, 'AUTH_EMAIL_DOMAINS', None))

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    def save(self, *args, **kwargs):
        self.email = self.email.lower().strip()
        self.username = self.email
        super().save(*args, **kwargs)


class Group(DefaultGroup):
    pass
