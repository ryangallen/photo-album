from django.conf import settings
from django.core.management.base import BaseCommand
from django.db.utils import IntegrityError

from users.models import User

class Command(BaseCommand):
    help = 'Creates a default superuser admin account'

    def handle(self, *args, **options):
        if settings.DEBUG == False and not settings.TESTING:
            self.stdout.write(self.style.NOTICE(
                "The 'create_default_superuser' command is not intended for use "
                "in production."))
            return

        try:
            user = User.objects.create_superuser(
                'admin@' + settings.AUTH_EMAIL_DOMAINS[0], 'photoalbumadmin1')
            self.stdout.write(self.style.SUCCESS("Superuser 'admin' created."))
        except IntegrityError:
            self.stdout.write(self.style.NOTICE("Superuser 'admin' already exists."))
