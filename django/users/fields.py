from django.db.models import EmailField

from .validators import LimitedDomainsEmailValidator


class LimitedDomainsEmailField(EmailField):

    def __init__(self, *args, **kwargs):
        domains = kwargs.pop('domains', None)
        self.default_validators = [LimitedDomainsEmailValidator(domains=domains)]
        super().__init__(*args, **kwargs)
