import re

from django.core.validators import EmailValidator, _lazy_re_compile


class LimitedDomainsEmailValidator(EmailValidator):
    '''https://github.com/django/django/blob/master/django/core/validators.py#L164'''

    def __init__(self, message=None, code=None, whitelist=None, domains=None):
        super().__init__(message, code, whitelist)
        if domains:
            self.domain_regex = _lazy_re_compile(
                '|'.join([re.escape(domain) for domain in domains]), re.IGNORECASE)
            self.message = '{message} ({domains})'.format(
                message=self.message, domains=', '.join('@' + domain for domain in domains))
