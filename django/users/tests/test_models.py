from django.conf import settings
from django.core.exceptions import ValidationError
from django.test import TestCase

from users.models import User


class UserModelsTest(TestCase):

    def setUp(self):
        self.valid_email = 'test@' + settings.AUTH_EMAIL_DOMAINS[0]

    def test_user_clean_fields_requires_email(self):
        with self.assertRaises(ValidationError) as cm:
            user = User(username='test', password='password')
            user.clean_fields()
        self.assertDictEqual(cm.exception.message_dict,
                             {'email': ['This field cannot be blank.']})

        user = User(username='test', email=self.valid_email, password='password')
        user.clean_fields()

    def test_email_must_have_domain_in_auth_email_domains_setting(self):
        with self.assertRaises(ValidationError) as cm:
            user = User(username='test', email='test@gmail.com', password='password')
            user.clean_fields()
        self.assertDictEqual(
            cm.exception.message_dict,
            {'email': ['Enter a valid email address. ({})'.format(
                ', '.join('@' + domain for domain in settings.AUTH_EMAIL_DOMAINS))]})

        for i, domain in enumerate(settings.AUTH_EMAIL_DOMAINS):
            user = User(username='test{}'.format(i),
                        email='test@{}'.format(domain),
                        password='password')
            user.clean_fields()

    def test_user_manager_create_user_requires_email(self):
        with self.assertRaises(ValueError) as cm:
            User.objects.create_user(None, 'testing123')
        self.assertEqual(str(cm.exception), 'An email must be set')

        self.assertFalse(User.objects.first())
        User.objects.create_user(self.valid_email, 'testing123')
        self.assertTrue(User.objects.first())
